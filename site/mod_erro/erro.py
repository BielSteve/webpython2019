#coding: utf-8
from flask import Blueprint, render_template, request
erro = Blueprint('erro', __name__, url_prefix='/erro', template_folder='templates')

@erro.route('/404')
def nao_encontrado():
    return render_template("form404.html"),404

@erro.route('/500')
def problema_servidor():
    return render_template("form500.html"), 500