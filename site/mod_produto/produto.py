#coding: utf-8
from flask import Blueprint, render_template
from mod_login.login import validaSessao
produto = Blueprint('produto', __name__, url_prefix='/produto', template_folder='templates')

@produto.route("/", methods=['GET','POST'])
@validaSessao
def produtos():
    return render_template('formListaProdutos.html')

@produto.route("/cad")
def cad():
    return render_template('formProduto.html')