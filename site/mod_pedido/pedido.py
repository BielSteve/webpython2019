#coding: utf-8
from flask import Blueprint, render_template
from mod_login.login import validaSessao
pedido = Blueprint('pedido', __name__, url_prefix='/pedido', template_folder='templates')

@pedido.route("/", methods=['GET','POST'])
@validaSessao
def pedidos():
    return render_template('formListaPedidos.html')

@pedido.route("/cad")
def cad():
    return render_template('formPedido.html')