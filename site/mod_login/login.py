#coding: utf-8
from flask import Blueprint, render_template, url_for, request, redirect, flash, session
from flask_wtf import FlaskForm
from functools import wraps
from wtforms import Form, BooleanField, StringField, PasswordField, validators
from wtforms.validators import DataRequired



login = Blueprint('login', __name__, url_prefix='/', template_folder='templates')



class RegistrationForm(FlaskForm):
    
    
    email = StringField('Email Address', 
        validators = [DataRequired(message="Campo obrigatório")
        ],
        render_kw={'class':'form-control'}
        )
    password = PasswordField('New Password', 
        validators=[DataRequired(message="Campo obrigatório")
        ],
        render_kw={'class':'form-control'}
        )
    



@login.route("/", methods=['GET','POST'])
def inicio():
    form = RegistrationForm(request.form)
    if form.validate_on_submit():
        
        if request.form.get('email')=='abcbolinhas' and form.password.data=='123':
            session.clear()
            session['usuario'] = request.form.get('email')
            return redirect(url_for('home.inicial'))
        else:
            flash('Credenciais inválidas')

                    
        
    return render_template('login.html', form=form)
            
        
@login.route("/")
def logoff():
    session.clear('usuario',None)
    return redirect( url_for('login.inicio') )    


# valida se o usuário esta ativo na sessão
def validaSessao(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'usuario' not in session:
#descarta os dados copiados da função original e retorna a tela de login
            return redirect(url_for('login.inicio',falhaSessao=1))
        else:
#retorna os dados copiados da função original
            return f(*args, **kwargs)

#retorna o resultado do if acima
    return decorated_function

