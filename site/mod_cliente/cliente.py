#coding: utf-8
from flask import Blueprint, render_template, request, url_for
from mod_login.login import validaSessao
cliente = Blueprint('cliente', __name__, url_prefix='/cliente', template_folder='templates')

@cliente.route("/", methods=['GET','POST'])
@validaSessao
def clientes():
    return render_template('formListaClientes.html')

@cliente.route("/cad", methods=['GET','POST'])
def cad():
    return render_template('formCliente.html')