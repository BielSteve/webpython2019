#coding: utf-8
from flask import Flask, redirect, request, session
import os
from datetime import timedelta
from mod_home.home import home
from mod_cliente.cliente import cliente
from mod_produto.produto import produto
from mod_pedido.pedido import pedido
from mod_erro.erro import erro
from mod_login.login import login




def create_app():
    app = Flask(__name__)
   # app.config['SECRET_KEY']='JOAO'
    app.secret_key = os.urandom(12).hex()

    app.register_blueprint(home)
    app.register_blueprint(cliente)
    app.register_blueprint(produto)
    app.register_blueprint(pedido)
    app.register_blueprint(erro)
    app.register_blueprint(login)


    @app.before_request
    
    def before_request():
        session.permanent = True
        app.permanent_session_lifetime = timedelta(seconds=60)

    @app.errorhandler(404)
    def nao_encontrado(error):
        return redirect("/erro/404",code=302)

    @app.errorhandler(500)
    def problema_servidor(error):
        return redirect("/erro/500",code=302), 500

    @app.route("/resultado/", methods=['GET','POST'])
    def resultado():
        
        
        if request.method == "GET":
            auxNome = request.args.get('nome')
            auxIdade = request.args.get('idade')
            return "Nome = {}<br>Idade:{}".format(auxNome,auxIdade), 200
        elif request.method == "POST":
            auxCod = request.form['cod']
            auxTel = request.form['tel']
            auxEmail = request.form['email']
            auxSenha = request.form['senha']
            auxEnder = request.form['ender']
            auxNur = request.form['nur']
            auxObs = request.form['obs']
            auxBairro = request.form['bairro']
            auxCidade = request.form['cidade']
            auxEstado = request.form['estado']
            auxCep = request.form['cep']
            auxNome = request.form['nome']
            

            return "Codigo = {}<br>Telefone:{}<br>Email:{}<br>Senha:{}<br>Endereço:{}<br>Numer:{}<br>Obs:{}<br>Bairro:{}<br>Cidade:{}<br>Estado:{}<br>CEP:{}<br>Nome:{}".format(auxCod, auxTel, auxEmail, auxSenha, auxEnder, auxNur, auxObs, auxBairro, auxCidade, auxEstado, auxCep, auxNome), 200
        else:
            return "Não definido", 200


    return app